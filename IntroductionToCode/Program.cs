// Tohle je komentář. Jakýkoliv text na tomto řádku bude počítačem ignorován. 
// V komentářích naleznete jak vysvětlivky tak zadání pro Vaši práci.


// Na dalším řádku je příkaz. Říká počítači, aby vypsal text v uvozovkách na obrazovku.
Console.WriteLine("Hello World.");

// Zkuste program spustit. Tlačítko se zelenou šipkou nahoře nebo F5.


// Pokud "zakomentujete" kus kódu, počítač ho bude brát jako komentář a tím pádem ignorovat. Zkuste z následujícího řádku odmazat lomítka a program znovu spustit.
//Console.WriteLine("How are you doing, World?");


// Následující příkazy jsou dost podobné těm předchozím. Odkomentujte následující dva řádky a spusťte program. Dokážete odhadnout, v čem se příkazy liší?
//Console.Write("First line, ");
//Console.WriteLine("and second line?");

// Tušíte? Rozdíl je v odřádkování (Enter). “WriteLine” vypíše text a na konci zmáčkne Enter, zatímco “Write” vypíše text a zůstane na řádku.
// V programování existuje vždy více než jedna cesta k cíli.
// Odkomentujte a spusťte následující blok, kde dosáhneme stejného výsledku na tři způsoby
// "Blok" komentáře začíná a končí lomítkem s hvězdičkou (/* */). Ty stačí smazat. 

/*
Console.Write("My first line.");
Console.WriteLine();
Console.Write("Second line and an empty line after this one.");
Console.WriteLine();
Console.WriteLine();


Console.WriteLine("My first line.");
Console.WriteLine("Second line and an empty line after this one.");
Console.WriteLine();


Console.Write("My first line.\nSecond line and an empty line after this one.\n\n");
*/

// Který způsob Vám vyhovuje nejvíce?


// Nyní na libovolném řádku smažte středník a zkuste program spustit.
// Vyskočí Vám informace, že program se nepodařilo spustit.
// Správná volba je v tuto chvíli vždy "No"/"Ne", aneb nechci spustit poslední funkční verzi.
// Zároveň uvidíte dole error s užitečným popisem a v kódu červenou vlnovku. Obojí Vám ušetří čas a nervy.
// Věnujte jim vždy pozornost


// Bez kopírování napište kód, který na první řádek vypíše Vaše jméno a na druhý Vaši náladu od 1 do 10.
// Visual studio Vám bude napovídat. Často uvidíte šedý návrh kódu.
// Klávesou "Tab" takový kód potvrdíte. 











