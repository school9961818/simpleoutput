## Prerequisites

[Visual Studio](https://visualstudio.microsoft.com/downloads/)

Podrobný návod pro instalaci naleznete třeba [zde](https://www.guru99.com/download-install-visual-studio.html). Nejdůležitější je **bod 6**. Tento kurz je aktuálně ve verzi **.Net framework 4.6**

## Goal

* Code
  * Console   

* Visual Studio
   * Error message
   * IntelliSense 

* Gitlab
  * Clone repository

## Diffuculty
* Beginners
* 1-2x45 min


## Instructions
Spusťe visual studio a na první stránce zvolte `Clone Repo`

![Clone_vs](IntroductionToCode/images/clone_vs.jpg)

Poté na této stránce nahoře stiskněte tlačítko `Clone` a zkopírujte https adresu

![Clone_gitlab](IntroductionToCode/images/clone_gitlab.jpg)

Adresu vložte do visual studia

![Clone_input](IntroductionToCode/images/clone_input.jpg)

Pokud po naklonování uvidíte na pravé straně něco takového

![Folder_view](IntroductionToCode/images/folder_view.jpg)

Stačí dvakrát kliknout na soubor s příponou `.sln` (Solution). Na konci byste měli vidět toto

![Solution_view](IntroductionToCode/images/solution_view.jpg)

Nyní stačí dvakrát kliknout na `Program.cs`. 
Zde najdete další instrukce.

## Feedback

https://forms.gle/4ksjcFQjLQqEL2Zd7

## Where to next

[Variables](https://gitlab.com/school9961818/variables)
